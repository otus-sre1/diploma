---
.prepare_script: &prepare_script
  - kubectl config use-context ${CI_PROJECT_PATH}:k3d
  - chmod 600 /builds/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.tmp/KUBECONFIG

.deploy_monitoring: &deploy_monitoring
  image: alpine/k8s:1.26.7
  stage: deploy-monitoring
  variables:
    DOLLAR: "$"
  script:
    - envsubst < devops/k8s/monitoring/prometheus/helm/values.yaml > /tmp/values.yaml
    - |
      helm upgrade prometheus kube-prometheus-stack \
        --install \
        --repo https://prometheus-community.github.io/helm-charts \
        --namespace monitoring \
        --create-namespace \
        --version 48.4.0 \
        --set kube-state-metrics.extraArgs='{--metric-labels-allowlist=pods=[*]}' \
        --values /tmp/values.yaml \
        --wait 1> /dev/null
    - kubectl apply --filename devops/k8s/monitoring/grafana/dashboards --recursive
    - |
      helm upgrade ingress-nginx-controller ingress-nginx \
        --install \
        --repo https://kubernetes.github.io/ingress-nginx \
        --namespace ingress-nginx-controller \
        --create-namespace \
        --version 4.7.1 \
        --values devops/k8s/website/ingress-controller/helm/values.yaml \
        --wait 1> /dev/null
    - kubectl create namespace cert-manager --dry-run=client --output yaml | kubectl apply --filename -
    - |
      kubectl apply --validate=false \
        --filename https://github.com/jetstack/cert-manager/releases/download/v1.12.2/cert-manager.crds.yaml
    - envsubst < devops/k8s/website/letsencrypt/letsencrypt-staging.yaml > /tmp/letsencrypt-staging.yaml
    - kubectl apply --filename /tmp/letsencrypt-staging.yaml
    # - envsubst < devops/k8s/website/letsencrypt/letsencrypt-production.yaml > /tmp/letsencrypt-production.yaml
    # - kubectl apply --filename /tmp/letsencrypt-production.yaml
    - |
      helm upgrade cert-manager cert-manager \
        --install \
        --repo https://charts.jetstack.io \
        --namespace cert-manager \
        --create-namespace \
        --version v1.12.3 \
        --values devops/k8s/website/cert-manager/helm/values.yaml \
        --wait 1> /dev/null
    - |
      helm upgrade loki loki-stack \
        --install \
        --repo https://grafana.github.io/helm-charts \
        --namespace monitoring \
        --create-namespace \
        --version 2.9.11 \
        --values devops/k8s/monitoring/loki/helm/values.yaml \
        --wait 1> /dev/null
    - envsubst < devops/k8s/monitoring/coroot/helm/values.yaml > /tmp/values.yaml
    - |
      helm upgrade coroot coroot \
        --install \
        --repo https://coroot.github.io/helm-charts \
        --namespace monitoring \
        --create-namespace \
        --version 0.3.9 \
        --values /tmp/values.yaml \
        --wait 1> /dev/null
    - |
      kubectl wait pods --timeout=300s --selector app.kubernetes.io/name=grafana \
        --for jsonpath='{.status.conditions[1].status}'=True --namespace monitoring && sleep 30s
    - WAN_IP=$(kubectl get ingress prometheus-grafana --namespace monitoring -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
    - |
      CFRECORD_TYPE=A
      CFRECORD_ID=$(curl -s \
        -X GET "https://api.cloudflare.com/client/v4/zones/${CFZONE_ID}/dns_records?name=${CFRECORD_NAME_GR}" \
        -H "X-Auth-Email: ${CFUSER}" -H "X-Auth-Key: ${CFKEY}" -H "Content-Type: application/json" \
        | jq -r '.result[0].id')
    - |
      curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/${CFZONE_ID}/dns_records/${CFRECORD_ID}" \
        -H "X-Auth-Email: ${CFUSER}" \
        -H "X-Auth-Key: ${CFKEY}" \
        -H "Content-Type: application/json" \
        --data "{\"id\":\"${CFZONE_ID}\",\"type\":\"${CFRECORD_TYPE}\",\"name\":\"${CFRECORD_NAME_GR}\",\"content\":\"${WAN_IP}\"}"
    - |
      kubectl wait pods --timeout=300s --selector app.kubernetes.io/name=coroot \
        --for jsonpath='{.status.conditions[1].status}'=True --namespace monitoring && sleep 30s
    - WAN_IP=$(kubectl get ingress coroot --namespace monitoring -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
    - |
      CFRECORD_TYPE=A
      CFRECORD_ID=$(curl -s \
        -X GET "https://api.cloudflare.com/client/v4/zones/${CFZONE_ID}/dns_records?name=${CFRECORD_NAME_CR}" \
        -H "X-Auth-Email: ${CFUSER}" -H "X-Auth-Key: ${CFKEY}" -H "Content-Type: application/json" \
        | jq -r '.result[0].id')
    - |
      curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/${CFZONE_ID}/dns_records/${CFRECORD_ID}" \
        -H "X-Auth-Email: ${CFUSER}" \
        -H "X-Auth-Key: ${CFKEY}" \
        -H "Content-Type: application/json" \
        --data "{\"id\":\"${CFZONE_ID}\",\"type\":\"${CFRECORD_TYPE}\",\"name\":\"${CFRECORD_NAME_CR}\",\"content\":\"${WAN_IP}\"}"

deploy_monitoring:
  <<: *deploy_monitoring
  before_script:
    - *prepare_script
  when: manual
  allow_failure: false
  only:
    - main
