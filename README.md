# Проект

## Внедрение SRE практик в стеке Wordpress, MySQL, memcached на базе GKE

## Цель

Вы первый SRE в компании, которая построена вокруг блога.

GCP, ingress nginx, wordpress, mysql, memcached.

## Описание/Пошаговая инструкция выполнения домашнего задания

### Внедряем

#### Первый этап

- [x] [Запустить DevOps CI/CD для вашего кода](https://gitlab.com/otus-sre1/diploma/-/blob/main/.gitlab)

- [x] Automate infrastructure scaling

  [VPA](https://gitlab.com/otus-sre1/diploma/-/blob/main/devops/k8s/website/ingress-controller/helm/values.yaml?ref_type=heads#L21-27)

  [HPA, resource metrics](https://gitlab.com/otus-sre1/diploma/-/blob/main/devops/k8s/website/wordpress/helm/values.yaml#L41)

  [HPA, prometheus metrics](https://gitlab.com/otus-sre1/diploma/-/blob/main/devops/k8s/website/ingress-controller/ingress-nginx-scaled-object.yaml)

- [x] Как вы будете соблюдать правило 50/50 (стратегия)

- [x] [Проведите SRE тренинг для других команд (презентация)](https://gitlab.com/otus-sre1/diploma/-/blob/main/docks/Presentation.md)

#### Второй этап

- [x] Определите [SLA,SLO,SLI](https://gitlab.com/otus-sre1/diploma/-/blob/main/docks/SLx.md). Чем вы руководствовались, с кем вы общались в компании?

- [x] Измерьте поведение вашей системы на соответствие SL*. Как вы этого добьетесь?

- [x] Определите [Error Budget](https://gitlab.com/otus-sre1/diploma/-/blob/main/docks/SLx.md) (velocity vs quality)

- [x] [Определите OKR для SRE](https://gitlab.com/otus-sre1/diploma/-/blob/main/docks/OKR.md)

#### Третий этап

- [x] [Внедрите мониторинг](https://gitlab.com/otus-sre1/diploma/-/blob/main/devops/k8s/monitoring)

- [x] [Golden signals: latency, saturation, traffic, errors]((https://gitlab.com/otus-sre1/diploma/-/blob/main/devops/k8s/monitoring))

- [x] В процессе нагрузочного тестирования возникли проблемы HPA WordPress. Выпущен [Blameless postmortem](https://gitlab.com/otus-sre1/diploma/-/blob/main/docks/Postmortem.md)

- [x] [Исправьте архитектуру, чтобы этого больше не происходило](https://gitlab.com/otus-sre1/diploma/-/blob/main/devops/k8s/website/nfs-server)

#### Четвертый этап

- [x] Проверьте, что при скалировании больше не происходит падение WordPress. Проведите эксперимент

- [x] Обсуждение улучшений

#### Create local k3s cluster

```shell
k3d cluster create --config devops/k3d/config.yaml && \
  kubectl config use-context k3d-3-node-cluster
```
